-- create Movie(movie_id, movie_title, movie_release_date,movie_time,director_name) table inside that

create table movie(
    movie_id integer primary key auto_increment, 
    movie_title varchar(50), 
    movie_release_date date,
    movie_time varchar(20),
    director_name varchar(50)
);

insert into movie(movie_id,movie_title,movie_release_date,movie_time,director_name) values(default,"pk","2019-05-03","10.30AM","omkar");
insert into movie(movie_id,movie_title,movie_release_date,movie_time,director_name) values(default,"babubali","2018-09-10","3.30PM","Suyog");
insert into movie(movie_id,movie_title,movie_release_date,movie_time,director_name) values(default,"pushpa","2021-01-02","12.00PM","abhi");