
const express=require('express')
const db=require('../db')
const utils=require('../utils')

const router=express.Router()

router.post('/',(request,response)=>{
    const{title,date,time,name}=request.body

    const query=`
    insert into movie
    (movie_id,movie_title,movie_release_date,movie_time,director_name) 
    values
    (default,'${title}','${date}','${time}','${name}');
    `
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.get('/',(request,response)=>{

    const query=`
    SELECT * FROM movie
    `
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.put('/:id',(request,response)=>{

    const{id}=request.params

    const{title,date,time,name}=request.body

    const query=`
    UPDATE movie
    SET
    movie_title='${title}',
    movie_release_date='${date}',
    movie_time='${time}',
    director_name='${name}'
    WHERE
    movie_id=${id}
        
    `
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.delete('/:id',(request,response)=>{
    const{id}=request.params
    const query=`
    DELETE FROM movie
    WHERE
    movie_id=${id}
    `
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})


module.exports=router


